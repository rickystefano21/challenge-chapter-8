import React, { useState } from 'react'
 
const AddData = props => {
const initialFormState = { id: null, username: '', email: '', experience:'' , level:''}
const [Userdata, setUserdata] = useState(initialFormState)
 
const handleInputChange = event => {
const { name, value } = event.target
 
setUserdata({ ...Userdata, [name]: value })
}
 
return (
<form className='form' onSubmit={event => {
event.preventDefault()
if (!Userdata.username || !Userdata.email || !Userdata.experience || !Userdata.level) return
 
props.AddData(Userdata)
setUserdata(initialFormState)
}}>
<div className='form'>
<input type="text" name="username" placeholder="Enter Username" value={Userdata.username} onChange={handleInputChange} />
<input type="text" name="email" placeholder="Enter Email" value={Userdata.email} onChange={handleInputChange} />
<input type="text" name="experience" placeholder="Enter Experience" value={Userdata.experience} onChange={handleInputChange}/>
<input type="text" name="level" placeholder="Enter Level" value={Userdata.level} onChange={handleInputChange}/>
<button className="btn btn-primary">Add User</button>
</div>
</form>
)
}
 
export default AddData;