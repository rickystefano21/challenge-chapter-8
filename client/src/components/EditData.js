import React, { useState, useEffect } from 'react'
 
const EditData = props => {
const [Userdata, setUserdata] = useState(props.currentData)
useEffect(
() => {
setUserdata(props.currentData)
},
[ props ]
)
 
const handleInputChange = event => {
const { name, value } = event.target
 
setUserdata({ ...Userdata, [name]: value })
}
 
return (
<form onSubmit={event => {
event.preventDefault()
if (!Userdata.username || !Userdata.email || !Userdata.experience || !Userdata.level) return
 
props.updateData(Userdata.id, Userdata)
}}>
<input type="text" name="username" placeholder="Enter Username" value={Userdata.username} onChange={handleInputChange} />
<input type="text" name="email" placeholder="Enter Email" value={Userdata.email} onChange={handleInputChange} />
<input type="text" name="experience" placeholder="Enter Experience" value={Userdata.experience} onChange={handleInputChange}/>
<input type="text" name="level" placeholder="Enter Level" value={Userdata.level} onChange={handleInputChange}/>
<button className="btn btn-primary">Edit Data</button>
<button onClick={() => props.setEditing(false)} className="btn btn-info">
Cancel
</button>
</form>
)
}
 
export default EditData;