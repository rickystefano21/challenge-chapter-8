import React from 'react';



const FilteredData = props =>(
<div>
<table>
<thead>
<tr>
<th>Username</th>
<th>Email</th>
<th>Experience</th>
<th>Level</th>
<th>Actions</th>
</tr>
</thead>
<tbody>
{
props.filteredData.map(item => (
<tr key={item.id}>
<td>{item.username}</td>
<td>{item.email}</td>
<td>{item.experience}</td>
<td>{item.level}</td>
<td>
<button onClick={() => { props.editData(item) }} className="btn btn-default">Edit</button>
<button onClick={() => { props.deleteData(item.id)}} className="btn btn-default">Delete</button>
</td>
</tr>
))
}
 
</tbody>
</table></div>
)

export default FilteredData;