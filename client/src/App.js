import React, { useState , useEffect  } from 'react';
import './App.css';
import AddData from './components/AddData';
import EditData from './components/EditData';
import FilteredData from './components/FilteredData';

const App= () => {
const Userdatalist = [
{ id: 1, username: 'aiueo', email: 'aiueo@gmail.com', experience: '700' , level:'12' },
{ id: 2, username: 'kucingterbang', email: 'kucingterbang@gmail.com' ,experience: '400' , level:'55'},
{ id: 3, username: 'kecoaksantet', email: 'kecoaksantet@yahoo.com',  experience: '500' , level:'88' },
{ id: 4, username: 'johnmayer', email: 'johnmayer@gmail.com', experience: '700' , level:'17' },
{ id: 5, username: 'chester', email: 'chester@gmail.com' ,experience: '400' , level:'44'},
{ id: 6, username: 'chris', email: 'chris@yahoo.com',  experience: '500' , level:'66' },
{ id: 7, username: 'avril', email: 'avril@gmail.com', experience: '700' , level:'23' },
{ id: 8, username: 'ricky', email: 'ricky@gmail.com' ,experience: '400' , level:'76'},
{ id: 9, username: 'stefano', email: 'stefano@yahoo.com',  experience: '500' , level:'77' },

]

const styles = {display:'inline',width:'30%',height:50,float:'left',padding:5,border:'0.5px solid black',marginBottom:10,marginRight:10}

const initialFormState = { id: null, username: '', email: '', experience: '' , level:'' }
const [Userdata, setUserdata] = useState(Userdatalist)
const [editing, setEditing] = useState(false);
const [currentData, setCurrentData] = useState(initialFormState)
const [inputText, setInputText] = useState("");
const [filteredData,setFilteredData] = useState(Userdata);
const inputHandler = (e) => {
  
  let lowerCase = e.target.value.toLowerCase();
  setInputText(lowerCase);
}
useEffect(() => {
  localStorage.setItem('userData', JSON.stringify(Userdata));
}, [Userdata]);

const addData = data => {

  data.id = Userdata.length + 1
  
  setUserdata([...Userdata, data])
  
  }

  const deleteData = id => {
  setUserdata(Userdata.filter(data => data.id !== id))
  }
 
  const editData = data => {
  setEditing(true)
  setCurrentData({
  id: data.id,
  username: data.username,
  email: data.email,
  experience : data.experience,
  level : data.level
  })
  }

  const updateData = (id, updatedData) => {
  setEditing(false)
  setUserdata(Userdata.map(item => (item.id === id ? updatedData : item)))
  }

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = Userdata.filter((data) => {return data.username.search(value) != -1;});
    setFilteredData(result);
  }

  
  const handleSearch1 = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = Userdata.filter((data) => {return data.experience.search(value) != -1;});
    setFilteredData(result);
  }

  
  const handleSearch2 = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = Userdata.filter((data) => {return data.email.search(value) != -1;});
    setFilteredData(result);
  }
  
  const handleSearch3 = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = Userdata.filter((data) => {return data.level.search(value) != -1;});
    setFilteredData(result);
  }
return (
<div className="container">
<h2 className="header">List of User</h2>
<div>
{editing ? (
<div>
<h2 className="sub-header2">Edit Data</h2>
<div>
<EditData
editing={editing}
setEditing={setEditing}
currentData={currentData}
updateData={updateData}
/>
</div>
</div>
) : (
<div>
<h3 className="sub-header2">Add User</h3>
<div>
<AddData AddData={addData} />
</div>
</div>
)}
<hr/>
<h3 className="sub-header">User Data</h3>
<div>
<FilteredData filteredData={filteredData} editData={editData} deleteData={deleteData}/>
</div>

<hr/>
<div>
<label>Search By Username:</label>
<input type="text" onChange={(event) =>handleSearch(event)} />
</div>

<div>
<label>Search By Experience:</label>
<input type="text" onChange={(event) =>handleSearch1(event)} />
</div>


<div>
<label>Search By Email:</label>
<input type="text" onChange={(event) =>handleSearch2(event)} />
</div>

<div>
<label>Search By Level:</label>
<input type="text" onChange={(event) =>handleSearch3(event)} />
</div>

</div>
</div>



);
}

export default App;
 
